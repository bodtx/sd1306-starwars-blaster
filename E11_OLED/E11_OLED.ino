/*
    Basé sur https://github.com/lexus2k/lcdgfx
    affiche un chargeur façon star wars sur un ssd1306 OLED.
*/
/**
     Attiny3216 PINS
                  ____
      5V      -|    |- GND
  shoot     -|    |-
   Mode    -|    |-
                 -|    |-
                 -|    |-
                 -|    |-
                 -|    |-
                 -|    |-
                 -|    |-
     SDA    -|___|- SCK

     shoot connected directly on the pcb's trigger pin
     mode connected directly on the pcb's shoot mode pin
*/



#include "lcdgfx.h"
#include <EEPROM.h>

const uint8_t Owl [] PROGMEM = {0x00, 0x00, 0x00, 0x80, 0xE0, 0xF0, 0x30, 0x18, 0x0C, 0x0C, 0x0E, 0x0E, 0x0E, 0x1E, 0x3F, 0xFF,
                                0xFF, 0xFF, 0xFE, 0xFE, 0xFE, 0xFE, 0xEC, 0xA0, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                0x40, 0x7C, 0x7F, 0x7F, 0x7F, 0x7F, 0x7C, 0x70, 0x70, 0x60, 0x60, 0x60, 0x70, 0x78, 0x7C, 0x7F,
                                0x7F, 0x7F, 0x7F, 0x7F, 0x7F, 0x7F, 0x7F, 0x7D, 0x7D, 0x6D, 0x6C, 0x60, 0x60, 0x60, 0x60, 0x40,
                                0x02, 0x3E, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE,
                                0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0x7E, 0x7E, 0x3E, 0x3E, 0x36, 0x36, 0x06, 0x06, 0x06, 0x02,
                                0x00, 0x00, 0x00, 0x01, 0x07, 0x0F, 0x0F, 0x1F, 0x3F, 0x3F, 0x7F, 0x7F, 0x7F, 0x7F, 0xFF, 0xFF,
                                0xFF, 0xFF, 0x7F, 0x1F, 0x1F, 0x1F, 0x0F, 0x07, 0x07, 0x06, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00
                               };

DisplaySSD1306_128x64_I2C display(-1); // or (-1,{busId, addr, scl, sda, frequency})

char buffer [12];
byte chargeur = 40;
int totalTir = 0;
unsigned long lastRechargeTime = 0;
const unsigned long rechargeDelay = 300;
unsigned long lastDebounceTime = 0;
const unsigned long debounceDelay = 200;
const int butPin = 0;
const int butPinSwitch = 1;
bool modeDisplay = 0;
byte animation = 0;

boolean animationIncrease = true;
NanoCanvas<64, 64, 1> canvas;
NanoCanvas<20, 40, 1> canvasP;
void setup()
{
  pinMode(butPin, INPUT);
  pinMode(butPinSwitch, INPUT);
  display.begin();
  display.clear();

  EEPROM.get(0, totalTir);
  //quand on éteind un parasite perturbe l'analogue read et fait un faux tir
  totalTir--;
  display.setFixedFont( ssd1306xled_font6x8 );
  display.printFixedN (10, 0, "Shots", STYLE_NORMAL, 1);
  sprintf(buffer, " %d", totalTir);
  display.printFixedN (10, 30, buffer, STYLE_NORMAL, 1);
  delay(2000);


  display.drawBitmap1(94, 32, 32, 32, Owl);
  display.setFixedFont( ssd1306xled_font6x8 );
  display.printFixedN (64,  0, "E-11", STYLE_NORMAL, 1);

  canvas.setColor( 0xFF );

  canvasP.setColor( 0xFF );

}


void loop()
{





  if (analogRead(butPin) < 100) {
    if ((millis() - lastDebounceTime) > debounceDelay) {
      if (analogRead(butPin) < 100) {
        lastDebounceTime = millis();

        totalTir++;
        EEPROM.put(0, totalTir);
        tir(5);

      }
    }

  }




  if ((millis() - lastRechargeTime) > rechargeDelay && chargeur < 40) {
    lastRechargeTime = millis();
    chargeur++;
  }

  if (analogRead(butPinSwitch) < 100) {
    if (modeDisplay != 0) {
      display.clear();
      display.drawBitmap1(94, 32, 32, 32, Owl);
      display.setFixedFont( ssd1306xled_font6x8 );
      display.printFixedN (64,  0, "E-11", STYLE_NORMAL, 1);
      modeDisplay = 0;
    }
    afficheBar();
    drawScanner();
  }
  else {
    if (modeDisplay != 1) {
      display.clear();
      modeDisplay = 1;
    }
    afficheChargeur();
  }

  lcd_delay(15);


}

void afficheBar() {
  canvasP.clear();
  canvasP.fillRect(2, 64, 22, 40 - chargeur);
  display.drawCanvas(64, 24, canvasP);
}

void drawScanner() {
  canvas.clear();
  canvas.drawVLine(32, 0, 64);
  canvas.drawHLine(0, 32, 64);

  canvas.drawVLine(8, 29, 35);
  canvas.drawVLine(16, 29, 35);
  canvas.drawVLine(24, 29, 35);
  canvas.drawVLine(40, 29, 35);
  canvas.drawVLine(48, 29, 35);
  canvas.drawVLine(56, 29, 35);

  canvas.drawHLine(29, 8, 35);
  canvas.drawHLine(29, 16, 35);
  canvas.drawHLine(29, 24, 35);
  canvas.drawHLine(29, 40, 35);
  canvas.drawHLine(29, 48, 35);
  canvas.drawHLine(29, 56, 35);
  canvas.drawCircle(32, 32, animation);
  display.drawCanvas(0, 0, canvas);
  animation++;
  if (animation > 32) {
    animation = 0;
  }
}

void tir(byte coup) {


  while (coup > 0 && chargeur > 0)
  {
    coup--;
    chargeur--;
  }
}

void afficheChargeur() {
  if (chargeur <= 0) {
    display.setFixedFont(ssd1306xled_font8x16);
    display.fill(0x00);
    display.printFixedN(10, 0, "LOW", STYLE_NORMAL, 2);
    delay(1000);
    display.clear();
  } else {
    display.setFixedFont(digital_font5x7_123);
  }
  if (chargeur < 10)
  {
    buffer[0] = '0';
    buffer[1] = chargeur + '0';
    display.printFixedN (10, 0, buffer, STYLE_NORMAL, 3);
  }
  else {
    display.printFixedN (10, 0, itoa(chargeur, buffer, 10), STYLE_NORMAL, 3);
  }
}
